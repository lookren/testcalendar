package com.lgdanilina.testcalendar.app;

import android.app.Application;

import com.lgdanilina.testcalendar.di.AppComponent;
import com.lgdanilina.testcalendar.di.DaggerAppComponent;
import com.lgdanilina.testcalendar.di.modules.ContextModule;

public class App extends Application {
    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
