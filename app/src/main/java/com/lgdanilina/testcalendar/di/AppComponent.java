package com.lgdanilina.testcalendar.di;

import android.content.Context;

import com.lgdanilina.testcalendar.di.modules.CalendarApiModule;
import com.lgdanilina.testcalendar.di.modules.ContextModule;
import com.lgdanilina.testcalendar.di.modules.RetrofitModule;
import com.lgdanilina.testcalendar.presenter.CalendarPresenter;
import com.lgdanilina.testcalendar.services.CalendarApiMethods;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, CalendarApiModule.class, RetrofitModule.class})
public interface AppComponent {
    CalendarPresenter getCalendarPresenter();
    CalendarApiMethods getCalendarApiMethods();
    Context getContext();
}
