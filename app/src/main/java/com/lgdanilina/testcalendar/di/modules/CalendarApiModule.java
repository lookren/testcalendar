package com.lgdanilina.testcalendar.di.modules;

import com.lgdanilina.testcalendar.presenter.CalendarPresenter;
import com.lgdanilina.testcalendar.services.CalendarApiMethods;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class CalendarApiModule {

    @Provides
    @Singleton
    CalendarApiMethods provideApiMethods(Retrofit retrofit) {
        return retrofit.create(CalendarApiMethods.class);
    }

    @Provides
    @Singleton
    CalendarPresenter provideCalendarPresenter() {
        return new CalendarPresenter();
    }
}
