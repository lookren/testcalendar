package com.lgdanilina.testcalendar.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MonthData implements Parcelable {
    public int year;
    public int month;
    public int[] holidays;
    public int[] preHolidays;
    public double workHours40;
    public double workHours36;
    public double workHours24;

    public MonthData() {
    }

    protected MonthData(Parcel in) {
        year = in.readInt();
        month = in.readInt();
        holidays = in.createIntArray();
        preHolidays = in.createIntArray();
        workHours40 = in.readDouble();
        workHours36 = in.readDouble();
        workHours24 = in.readDouble();
    }

    public static final Creator<MonthData> CREATOR = new Creator<MonthData>() {
        @Override
        public MonthData createFromParcel(Parcel in) {
            return new MonthData(in);
        }

        @Override
        public MonthData[] newArray(int size) {
            return new MonthData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(year);
        dest.writeInt(month);
        dest.writeIntArray(holidays);
        dest.writeIntArray(preHolidays);
        dest.writeDouble(workHours40);
        dest.writeDouble(workHours36);
        dest.writeDouble(workHours24);
    }
}
