package com.lgdanilina.testcalendar.presenter;

import android.support.annotation.NonNull;

public interface BasePresenter<V> {

    void attachView(@NonNull V view);
    void detachView();
    void onDestroy();
}
