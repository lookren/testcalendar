package com.lgdanilina.testcalendar.presenter;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;

import com.lgdanilina.testcalendar.R;
import com.lgdanilina.testcalendar.app.App;
import com.lgdanilina.testcalendar.model.MonthData;
import com.lgdanilina.testcalendar.view.CalendarView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CalendarPresenter implements BasePresenter<CalendarView> {
    private CalendarView mCalendarView;

    public void refreshOrUpdateData(List<MonthData> monthDatas) {
        if (monthDatas == null || monthDatas.isEmpty()) {
            App.getAppComponent().getCalendarApiMethods().getCalendarData()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        if (mCalendarView != null) {
                            mCalendarView.setCalendarData(response.body);
                            updateCalendarData(response.body);
                        }
                    }, throwable -> {
                        throwable.printStackTrace();
                        if (mCalendarView != null) {
                            Snackbar.make(mCalendarView.getRootView(),
                                    throwable.getMessage(), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.retry, v -> refreshOrUpdateData(null)).show();
                        }
                    });
        } else {
            updateCalendarData(monthDatas);
        }
    }


    private void updateCalendarData(List<MonthData> monthDatas) {
        setCalendarHolidays(monthDatas);
        setCalendarPreHolidays(monthDatas);
        Date currentDate = mCalendarView.getCalendarView().getCurrentDate().getDate();
        mCalendarView.updateMonthInfo(currentDate, monthDatas);
        mCalendarView.getCalendarView().setOnMonthChangedListener((widget, date) -> mCalendarView.updateMonthInfo(date.getDate(), monthDatas));
        if (!monthDatas.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            MonthData minMonthData = monthDatas.get(0);
            calendar.set(minMonthData.year, minMonthData.month - 1, 0);
            int dayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
            calendar.set(minMonthData.year, minMonthData.month - 1, dayOfMonth);
            MaterialCalendarView.StateBuilder stateBuilder = mCalendarView.getCalendarView().newState();
            stateBuilder.setMinimumDate(calendar);
            MonthData maxMonthData = monthDatas.get(monthDatas.size() - 1);
            calendar.set(maxMonthData.year, maxMonthData.month - 1, 0);
            dayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            calendar.set(maxMonthData.year, maxMonthData.month - 1, dayOfMonth);
            stateBuilder.setMaximumDate(calendar).commit();
        }
    }

    private void setCalendarHolidays(List<MonthData> calendarData) {
        HashSet<CalendarDay> holidays = new HashSet<>();
        Calendar calendar = Calendar.getInstance();
        List<Date> dates = new ArrayList<>();
        Calendar calendarValue = Calendar.getInstance();
        for (MonthData each : calendarData) {
            for (int day : each.holidays) {
                calendarValue.set(each.year, each.month - 1, day);
                dates.add(calendarValue.getTime());
            }
        }
        for (Date date : dates) {
            calendar.setTime(date);
            CalendarDay calendarDay = CalendarDay.from(calendar);
            holidays.add(calendarDay);
        }
        mCalendarView.showHolidays(holidays);
    }

    private void setCalendarPreHolidays(List<MonthData> calendarData) {
        HashSet<CalendarDay> preHolidays = new HashSet<>();
        Calendar calendar = Calendar.getInstance();
        List<Date> dates = new ArrayList<>();
        Calendar calendarValue = Calendar.getInstance();
        for (MonthData each : calendarData) {
            if (each.preHolidays != null) {
                for (int day : each.preHolidays) {
                    calendarValue.set(each.year, each.month - 1, day);
                    dates.add(calendarValue.getTime());
                }
            }
        }
        for (Date date : dates) {
            calendar.setTime(date);
            CalendarDay calendarDay = CalendarDay.from(calendar);
            preHolidays.add(calendarDay);
        }
        mCalendarView.showPreHolidays(preHolidays);
    }

    @Override
    public void attachView(@NonNull CalendarView view) {
        mCalendarView = view;
    }

    @Override
    public void detachView() {
        mCalendarView = null;
    }

    @Override
    public void onDestroy() {
    }
}
