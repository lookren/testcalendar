package com.lgdanilina.testcalendar.services;

import retrofit2.http.GET;
import rx.Observable;

public interface CalendarApiMethods {
    @GET("calendar_data.txt")
    Observable<CalendarData> getCalendarData();
}
