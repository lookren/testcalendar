package com.lgdanilina.testcalendar.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lgdanilina.testcalendar.model.MonthData;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {
    public static String generateJson() {
        List<MonthData> data = new ArrayList<>();
        for (int i = 2016; i <= 2017; i++) {
            for (int month = 1; month <= 12; month++) {
                MonthData monthData = new MonthData();
                monthData.year = i;
                monthData.month = month;
                switch (monthData.year) {
                    case 2016:
                        switch (monthData.month) {
                            case 1:
                                monthData.holidays = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 16, 17, 23, 24, 30, 31};
                                monthData.workHours40 = 120;
                                monthData.workHours36 = 108;
                                monthData.workHours24 = 72;
                                break;
                            case 2:
                                monthData.holidays = new int[]{6, 7, 13, 14, 21, 22, 23, 27, 28};
                                monthData.preHolidays = new int[]{20};
                                monthData.workHours40 = 159;
                                monthData.workHours36 = 143;
                                monthData.workHours24 = 95;
                                break;
                            case 3:
                                monthData.holidays = new int[]{5, 6, 7, 8, 12, 13, 19, 20, 26, 27};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 4:
                                monthData.holidays = new int[]{2, 3, 9, 10, 16, 17, 23, 24, 30};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 5:
                                monthData.holidays = new int[]{1, 2, 3, 7, 8, 9, 14, 15, 21, 22, 28, 29};
                                monthData.workHours40 = 152;
                                monthData.workHours36 = 136.8;
                                monthData.workHours24 = 91.2;
                                break;
                            case 6:
                                monthData.holidays = new int[]{4, 5, 11, 12, 13, 18, 19, 25, 26};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 7:
                                monthData.holidays = new int[]{2, 3, 9, 10, 16, 17, 23, 24, 30, 31};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 8:
                                monthData.holidays = new int[]{6, 7, 13, 14, 20, 21, 27, 28};
                                monthData.workHours40 = 184;
                                monthData.workHours36 = 165.6;
                                monthData.workHours24 = 110.4;
                                break;
                            case 9:
                                monthData.holidays = new int[]{3, 4, 10, 11, 17, 18, 24, 25};
                                monthData.workHours40 = 176;
                                monthData.workHours36 = 158.4;
                                monthData.workHours24 = 105.6;
                                break;
                            case 10:
                                monthData.holidays = new int[]{1, 2, 8, 9, 15, 16, 22, 23, 29, 30};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 11:
                                monthData.holidays = new int[]{4, 5, 6, 12, 13, 19, 20, 26, 27};
                                monthData.preHolidays = new int[]{3};
                                monthData.workHours40 = 167;
                                monthData.workHours36 = 150.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 12:
                                monthData.holidays = new int[]{3, 4, 10, 11, 17, 18, 24, 25, 31};
                                monthData.workHours40 = 176;
                                monthData.workHours36 = 158.4;
                                monthData.workHours24 = 105.6;
                                break;
                        }
                        break;
                    case 2017:
                        switch (monthData.month) {
                            case 1:
                                monthData.holidays = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 14, 15, 21, 22, 28, 29};
                                monthData.workHours40 = 136;
                                monthData.workHours36 = 122.4;
                                monthData.workHours24 = 81.6;
                                break;
                            case 2:
                                monthData.holidays = new int[]{4, 5, 11, 12, 18, 19, 23, 24, 25, 26};
                                monthData.preHolidays = new int[]{22};
                                monthData.workHours40 = 143;
                                monthData.workHours36 = 128.6;
                                monthData.workHours24 = 85.4;
                                break;
                            case 3:
                                monthData.holidays = new int[]{4, 5, 8, 11, 12, 18, 19, 25, 26};
                                monthData.preHolidays = new int[]{7};
                                monthData.workHours40 = 175;
                                monthData.workHours36 = 157.4;
                                monthData.workHours24 = 104.6;
                                break;
                            case 4:
                                monthData.holidays = new int[]{1, 2, 8, 9, 15, 16, 22, 23, 29, 30};
                                monthData.workHours40 = 160;
                                monthData.workHours36 = 144;
                                monthData.workHours24 = 96;
                                break;
                            case 5:
                                monthData.holidays = new int[]{1, 6, 7, 8, 9, 13, 14, 20, 21, 27, 28};
                                monthData.workHours40 = 160;
                                monthData.workHours36 = 144;
                                monthData.workHours24 = 96;
                                break;
                            case 6:
                                monthData.holidays = new int[]{3, 4, 10, 11, 12, 17, 18, 24, 25};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 7:
                                monthData.holidays = new int[]{1, 2, 8, 9, 15, 16, 22, 23, 29, 30};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 8:
                                monthData.holidays = new int[]{5, 6, 12, 13, 19, 20, 26, 27};
                                monthData.workHours40 = 184;
                                monthData.workHours36 = 165.6;
                                monthData.workHours24 = 110.4;
                                break;
                            case 9:
                                monthData.holidays = new int[]{2, 3, 9, 10, 16, 17, 23, 24, 30};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 10:
                                monthData.holidays = new int[]{1, 7, 8, 14, 15, 21, 22, 28, 29};
                                monthData.workHours40 = 176;
                                monthData.workHours36 = 158.4;
                                monthData.workHours24 = 105.6;
                                break;
                            case 11:
                                monthData.holidays = new int[]{4, 5, 6, 11, 12, 18, 19, 25, 26};
                                monthData.preHolidays = new int[]{3};
                                monthData.workHours40 = 167;
                                monthData.workHours36 = 150.2;
                                monthData.workHours24 = 100.8;
                                break;
                            case 12:
                                monthData.holidays = new int[]{2, 3, 9, 10, 16, 17, 23, 24, 30, 31};
                                monthData.workHours40 = 168;
                                monthData.workHours36 = 151.2;
                                monthData.workHours24 = 100.8;
                                break;
                        }
                        break;
                }
                data.add(monthData);
            }
        }
        JsonArray jsonArray = new JsonArray();
        for (MonthData monthData : data) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("year", monthData.year);
            jsonObject.addProperty("month", monthData.month);
            JsonArray holidays = new JsonArray();
            if (monthData.holidays != null) {
                for (int each : monthData.holidays) {
                    holidays.add(each);
                }
            }
            jsonObject.add("holidays", holidays);
            JsonArray preHolidays = new JsonArray();
            if (monthData.preHolidays != null) {
                for (int each : monthData.preHolidays) {
                    preHolidays.add(each);
                }
            }
            jsonObject.add("preHolidays", preHolidays);
            jsonObject.addProperty("workHours40", monthData.workHours40);
            jsonObject.addProperty("workHours36", monthData.workHours36);
            jsonObject.addProperty("workHours24", monthData.workHours24);
            jsonArray.add(jsonObject);
        }
        JsonObject result = new JsonObject();
        result.add("body", jsonArray);
        return result.toString();
    }
}
