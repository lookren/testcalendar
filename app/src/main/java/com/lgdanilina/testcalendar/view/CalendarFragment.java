package com.lgdanilina.testcalendar.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import com.lgdanilina.testcalendar.model.MonthData;
import com.lgdanilina.testcalendar.R;
import com.lgdanilina.testcalendar.app.App;
import com.lgdanilina.testcalendar.presenter.BasePresenter;
import com.lgdanilina.testcalendar.presenter.CalendarPresenter;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

public class CalendarFragment extends Fragment implements CalendarView {

    private MaterialCalendarView mCalendarView;
    private LabeledTextView mDays;
    private LabeledTextView mDaysWork;
    private LabeledTextView mDaysOff;
    private LabeledTextView mCurrentQuarter;
    private LabeledTextView mCurrentHalfYear;
    private LabeledTextView mCurrentYear;
    private LabeledTextView mHoursWeek40;
    private LabeledTextView mHoursWeek36;
    private LabeledTextView mHoursWeek24;
    private View mRoot;

    private ArrayList<MonthData> mCalendarData;
    private CalendarPresenter mCalendarPresenter = App.getAppComponent().getCalendarPresenter();

    private static final String MONTH_FORMAT_TEMPLATE = "LLLL yyyy";
    private static final String CALENDAR_DATA_STATE_KEY = "CALENDAR_DATA_STATE_KEY";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_calendar, null, false);
        mRoot = parent.findViewById(R.id.root);
        Toolbar toolbar = (Toolbar) mRoot.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        mDays = (LabeledTextView) mRoot.findViewById(R.id.days_number);
        mDaysWork = (LabeledTextView) mRoot.findViewById(R.id.work_days_number);
        mDaysOff = (LabeledTextView) mRoot.findViewById(R.id.days_off_number);
        mCurrentYear = (LabeledTextView) mRoot.findViewById(R.id.current_year);
        mCurrentHalfYear = (LabeledTextView) mRoot.findViewById(R.id.current_half_year);
        mCurrentQuarter = (LabeledTextView) mRoot.findViewById(R.id.current_quarter);
        mHoursWeek40 = (LabeledTextView) mRoot.findViewById(R.id.work_hours_40);
        mHoursWeek36 = (LabeledTextView) mRoot.findViewById(R.id.work_hours_36);
        mHoursWeek24 = (LabeledTextView) mRoot.findViewById(R.id.work_hours_24);
        mCalendarView = (MaterialCalendarView) mRoot.findViewById(R.id.calendar_view);
        mCalendarView.setTopbarVisible(false);
        mCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_NONE);
        return mRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            mCalendarData = savedInstanceState.getParcelableArrayList(CALENDAR_DATA_STATE_KEY);
        }
        mCalendarPresenter.attachView(this);
        mCalendarPresenter.refreshOrUpdateData(mCalendarData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(CALENDAR_DATA_STATE_KEY, mCalendarData);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCalendarPresenter.detachView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCalendarPresenter.onDestroy();
    }

    public void updateMonthInfo(Date date, List<MonthData> calendarData) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(MONTH_FORMAT_TEMPLATE, Locale.getDefault());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(dateFormat.format(date));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        mDays.setValue(String.valueOf(daysInMonth));

        if (calendarData != null) {
            MonthData currentMonthData = null;
            for (MonthData monthData : calendarData) {
                int monthValue = calendar.get(Calendar.MONTH);
                if (calendar.get(Calendar.YEAR) == monthData.year && monthValue == monthData.month - 1) {
                    currentMonthData = monthData;
                }
            }
            if (currentMonthData != null) {
                mHoursWeek40.setValue(String.valueOf(currentMonthData.workHours40));
                mHoursWeek36.setValue(String.valueOf(currentMonthData.workHours36));
                mHoursWeek24.setValue(String.valueOf(currentMonthData.workHours24));
            }
            int daysOffInMonth = getOffDaysInMonth(currentMonthData);
            mDaysWork.setValue(String.valueOf(daysInMonth - daysOffInMonth));
            mDaysOff.setValue(String.valueOf(daysOffInMonth));
            mCurrentYear.setValue(String.valueOf(calendar.get(Calendar.YEAR)));
            mCurrentHalfYear.setValue(String.valueOf(calendar.get(Calendar.MONTH) >= 6 ? 2 : 1));
            mCurrentQuarter.setValue(String.valueOf(calendar.get(Calendar.MONTH) / 3 + 1));
        }
    }

    private static int getOffDaysInMonth(MonthData monthData) {
        if (monthData != null) {
            return monthData.holidays.length;
        }
        return 0;
    }

    @Override
    public void setCalendarData(ArrayList<MonthData> calendarData) {
        mCalendarData = calendarData;
    }

    @Override
    public MaterialCalendarView getCalendarView() {
        return mCalendarView;
    }

    @Override
    public View getRootView() {
        return mRoot;
    }

    @Override
    public void showHolidays(HashSet<CalendarDay> holidays) {
        mCalendarView.addDecorator(new EventDecorator(ContextCompat.getColor(getContext(), R.color.holidayColor),
                getContext().getResources().getDimensionPixelSize(R.dimen.radius),
                holidays));
    }

    @Override
    public void showPreHolidays(HashSet<CalendarDay> preHolidays) {
        mCalendarView.addDecorator(new EventDecorator(ContextCompat.getColor(getContext(), R.color.preHolidayColor),
                getContext().getResources().getDimensionPixelSize(R.dimen.radius),
                preHolidays));
    }
}
