package com.lgdanilina.testcalendar.view;

import android.view.View;

import com.lgdanilina.testcalendar.model.MonthData;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public interface CalendarView {
    void setCalendarData(ArrayList<MonthData> calendarData);
    MaterialCalendarView getCalendarView();
    View getRootView();
    void showHolidays(HashSet<CalendarDay> holidays);
    void showPreHolidays(HashSet<CalendarDay> holidays);
    void updateMonthInfo(Date date, List<MonthData> calendarData);
}
