package com.lgdanilina.testcalendar.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.HashSet;

class EventDecorator implements DayViewDecorator {
    private int mColor;
    private int mRadius;
    private HashSet<CalendarDay> mCalendarDayCollection;

    public EventDecorator(int color, int radius, HashSet<CalendarDay> calendarDayCollection) {
        mColor = color;
        mRadius = radius;
        mCalendarDayCollection = calendarDayCollection;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return mCalendarDayCollection.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new DaySpan(mColor, mRadius));
    }

    private class DaySpan implements LineBackgroundSpan {
        private final float radius;
        private final int color;

        DaySpan(int color, int radius) {
            this.radius = radius;
            this.color = color;
        }

        @Override
        public void drawBackground(
                Canvas canvas, Paint paint,
                int left, int right, int top, int baseline, int bottom,
                CharSequence charSequence,
                int start, int end, int lineNum) {
            int oldColor = paint.getColor();
            if (color != 0) {
                paint.setColor(color);
            }
            canvas.drawCircle((left + right) / 2, (bottom + top) / 2, radius, paint);
            paint.setColor(oldColor);
        }
    }
}

