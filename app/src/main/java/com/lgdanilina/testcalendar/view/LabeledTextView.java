package com.lgdanilina.testcalendar.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lgdanilina.testcalendar.R;

public class LabeledTextView extends RelativeLayout {

    private static final String TITLE_SEPARATOR = ": ";

    private TextView mDataView1;

    private int mTitleColor;
    private String mValueStr;
    private String mTitleStr;

    public LabeledTextView(Context context) {
        super(context);
        init(null, 0, 0);
    }

    public LabeledTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0, 0);
    }

    public LabeledTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LabeledTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, defStyleAttr, defStyleRes);
    }

    protected void init(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        View root = inflate(getContext(), R.layout.labeled_text_view, this);
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.LabeledTextView, defStyleAttr, defStyleRes);
        mTitleStr = a.getString(R.styleable.LabeledTextView_title_text);
        a.recycle();
        mTitleColor = ContextCompat.getColor(getContext(), android.R.color.darker_gray);
        mDataView1 = (TextView) root.findViewById(R.id.data_value1);
    }

    public void setValue(String textValue) {
        if (!TextUtils.isEmpty(textValue)) {
            mValueStr = textValue;
            setValueInternal();
            setVisibility(VISIBLE);
        } else {
            setVisibility(GONE);
        }
    }

    private void setValueInternal() {
        if (!TextUtils.isEmpty(mTitleStr)) {
            final String text = !TextUtils.isEmpty(mValueStr) ? mTitleStr.concat(TITLE_SEPARATOR).concat(mValueStr) : mTitleStr;
            final int separatorPosition = text.indexOf(TITLE_SEPARATOR);
            if (separatorPosition > -1) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
                final int spanIndex = separatorPosition + TITLE_SEPARATOR.length();
                spannableStringBuilder.setSpan(new ForegroundColorSpan(mTitleColor), 0, spanIndex, 0);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK), spanIndex, text.length(), 0);
                mDataView1.setText(spannableStringBuilder);
            } else {
                mDataView1.setText(text);
            }
        } else {
            mDataView1.setText(mValueStr);
        }
    }
}
