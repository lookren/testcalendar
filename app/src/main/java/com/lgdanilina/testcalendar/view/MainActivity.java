package com.lgdanilina.testcalendar.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lgdanilina.testcalendar.R;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
